package com.cse2010.group1;

import java.io.IOException;

import javax.jdo.annotations.PersistenceAware;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import OfflineDemo.DataStructures.NameSuggestion;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceAware
@SuppressWarnings("serial")
public class SubmitName extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String toName = req.getParameter("toName");
		Key k = KeyFactory.stringToKey(toName);
		Key player = (Key) req.getSession().getAttribute("player");
		NameSuggestion n = new NameSuggestion(
				req.getParameter("suggestedName"),
				player, k);
		PMF.getPM().makePersistent(n);
		resp.getWriter().println("<script>loadBody('explore.jsp');</script>");
		return;
	}
}
