package com.cse2010.group1;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import OfflineDemo.DataStructures.SolarSystem;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class TestGAEServlet extends HttpServlet {
	private SolarSystem[] galaxy;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// this servlet only responds to casey.
		if (req.getUserPrincipal() == null
				|| !req.getUserPrincipal().toString()
						.equals("wer4geeks@gmail.com")) {
			UserService userService = UserServiceFactory.getUserService();
			resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
			return;
		}

		req.getAttribute("command");
		resp.setContentType("text/html");
		// header
		resp.getWriter()
				.println(
						"<!DOCTYPE HTML><html><head><title>Hello App Engine</title></head><body><h1>Hello App Engine!</h1>");
		resp.getWriter()
				.println(
						"<table><td><form name=\"input\" action=\"testgae\" method=\"post\">Commands (semicolon separated): <input type=\"text\" name=\"command\"><input type=\"submit\" value=\"Submit\"></form></td></tr><tr><td><b>Server response:</b><br><pre>");
		// print output of last command
		try {
			parseCommand(req, resp);
		} catch (Exception ex) {
			// do nothing on null
		}
		resp.getWriter().println("Session ID: " + req.getSession().getId());
		resp.getWriter().println("User ID: " + req.getUserPrincipal());

		// print end of file
		resp.getWriter().println("</pre></td></tr></table></body></html>");

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}

	private void parseCommand(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {

		String[] commands = req.getParameter("command").split("[;,:]");

		String[] symbols = new String[] { // last one need always be argle
		"help", "halp", "generategalaxy", "alertall", "arglegarbleglyopglyph" };

		for (String command : commands) {
			String[] cleanedCommand = command.toLowerCase().split("\\s+");
			int sym = 0;
			for (; sym < symbols.length; sym++) {
				if (symbols[sym].startsWith(cleanedCommand[0])) {
					break;
				}
			}

			switch (sym) {

			case 0:
			case 1:
				// print help
				printHelp(resp);

				break;
			case 2:
				// generategalaxy
				// FIXME TODO:this needs to be in a background process, times
				// out.
				PersistenceManager pm = PMF.get().getPersistenceManager();
				//galaxy = OfflineDemo.GalaxyGenerator.generateGalaxy(resp);
				resp.getWriter().println(galaxy.length + " stars generated.");
				try {
					for (SolarSystem s : galaxy)
						pm.makePersistent(s);
				} finally {
					pm.close();
				}
				break;

			default:
				resp.getWriter().println("Unrecognized Command.");
				printHelp(resp);
			}

		}

	}

	private void printHelp(HttpServletResponse resp) throws IOException {
		resp.getWriter().println("Supported commands:");
		resp.getWriter().println("help: print this message.");
	}


}
