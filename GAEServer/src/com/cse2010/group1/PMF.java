package com.cse2010.group1;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public final class PMF {
    private static final PersistenceManagerFactory pmfInstance =
        JDOHelper.getPersistenceManagerFactory("transactions-optional");

    private PMF() {}

    private static PersistenceManager pm = pmfInstance.getPersistenceManager();
    public static PersistenceManagerFactory get() {
        return pmfInstance;
    }
    public static PersistenceManager getPM(){
    	if (pm.isClosed()){
    		pm = pmfInstance.getPersistenceManager();
    		return pm;
    	}
    	else{
    		
    		return pm;
    	}
    	
    }
}