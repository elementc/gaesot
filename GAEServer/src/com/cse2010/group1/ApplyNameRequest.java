package com.cse2010.group1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URI;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.PersistenceAware;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import OfflineDemo.Player;
import OfflineDemo.DataStructures.NameSuggestion;
import OfflineDemo.DataStructures.NameSuggestion.ReviewStatus;
import OfflineDemo.DataStructures.Nameable;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
@PersistenceAware
public class ApplyNameRequest extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		@SuppressWarnings("unchecked")
		Enumeration<String> e = req.getParameterNames();
		String directive;
		if (req.getParameter("approve") != null) {
			directive = "approve";
		} else {
			directive = "deny";
		}

		LinkedList<String> toActOn = new LinkedList<String>();

		while (e.hasMoreElements()) {
			String s = e.nextElement();
			if (s.equals("approve") || s.equals("deny"))
				continue;
			if (req.getParameter(s).equals("on"))
				toActOn.add(s);
		}
		for (String keyString : toActOn) {
			PersistenceManager PM = PMF.getPM();
			Key requestKey = KeyFactory.stringToKey(keyString);
			NameSuggestion n = PM.getObjectById(NameSuggestion.class,
					requestKey);

			if (n.getReviewStatus() == ReviewStatus.PENDING) {
				if (directive.equals("approve")) {
					approveItem(n);

				} else if (directive.equals("deny")) {
					denyItem(n);
				}
			}

		}
		resp.getWriter().println("<script>loadBody('ManageNameRequests.jsp');</script>");
		return;

	}

	public void denyItem(NameSuggestion n) {

		PersistenceManager PM = PMF.getPM();
		n.setReviewStatus(NameSuggestion.ReviewStatus.DENIED);
		PM.makePersistent(n);
		PM.close();
	}

	@SuppressWarnings("unchecked")
	public void approveItem(NameSuggestion n) {

		PersistenceManager PM = PMF.getPM();
		Nameable toName = PM.getObjectById(
				JSPUtilMethods.getNameableClassFromKey(n.getThingToName()),
				n.getThingToName());
		if (toName.getNamer() != null) {
			denyItem(n);
			return;
		}
		toName.setNamer(n.getPlayerWhoSuggested());
		toName.setPrettyName(n.getSuggestedName());
		n.setReviewStatus(NameSuggestion.ReviewStatus.ACCEPTED);
		PM.makePersistent(toName);
		PM.makePersistent(n);

		Player p = PM.getObjectById(Player.class, n.getPlayerWhoSuggested());

		try {

			URI u = new URI("https", "graph.facebook.com", "/"
					+ p.getFacebook_user_id() + "/notifications", null);
			URL url = u.toURL();
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");

			OutputStreamWriter writer = new OutputStreamWriter(
					connection.getOutputStream());
			writer.write("access_token="
					+ "436720203041582|TYG6ln9F5K5fdABSJFVs0Jv-O04"
					+ "&href=&template=Your%20discovery%20has%20been%20approved%20and%20recorded%20by%20the%20ISU.%20Play%20again%20now%2C%20and%20see%20what%20else%20is%20out%20there%20in%20space!");
			writer.close();

			System.out.println(connection.getResponseCode());
			BufferedReader r = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String l;
			while ((l = r.readLine()) != null) {
				System.out.println(l);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		PM.close();

		PM = PMF.getPM();
		Query q = PM.newQuery(NameSuggestion.class);
		q.setFilter("thingToName == :param1");
		List<NameSuggestion> nowRedundantSuggestions = (List<NameSuggestion>) q
				.execute(KeyFactory.keyToString(n.getThingToName()));
		for (NameSuggestion redundant : nowRedundantSuggestions) {
			denyItem(redundant);
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

}
