package com.cse2010.group1;

import java.io.IOException;
import java.util.List;

import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import OfflineDemo.Player;
import OfflineDemo.DataStructures.SolarSystem;

@SuppressWarnings("serial")
public class Explore extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		PMF.getPM().close();
		HttpSession ses = req.getSession();
		if (ses.getAttribute("player") == null) {
			throw new IOException("No Player Object in Session!");
		}
		Player p = (Player) PMF.getPM().getObjectById(Player.class,
				ses.getAttribute("player"));

		if (p.getLocation() == null) {
			Query q = PMF.getPM().newQuery(SolarSystem.class);
			q.setFilter("isStarterSystem==true");
			@SuppressWarnings("unchecked")
			List<SolarSystem> systems = (List<SolarSystem>) q.execute();
			p.setLocation(systems.get(0).getKey());

		}

		if (req.getParameter("jumpTarget") != null) {
			for (Key k : PMF.getPM().getObjectById(SolarSystem.class,
					p.getLocation()).nearbySystems) {

				if (req.getParameter("jumpTarget").equals(k.getName())) {

					p.setLocation(KeyFactory.createKey(
							SolarSystem.class.getSimpleName(),
							req.getParameter("jumpTarget")));
					break;

				}
			}
		}

		resp.getWriter().println("<script>loadBody('explore.jsp');</script>");
		return;

	}
}
