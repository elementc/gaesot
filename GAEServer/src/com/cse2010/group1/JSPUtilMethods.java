package com.cse2010.group1;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.PersistenceAware;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import OfflineDemo.DataStructures.AsteroidBelt;
import OfflineDemo.DataStructures.Moon;
import OfflineDemo.DataStructures.Nameable;
import OfflineDemo.DataStructures.Planet;
import OfflineDemo.DataStructures.SolarSystem;
import OfflineDemo.DataStructures.Station;
import OfflineDemo.DataStructures.SystemObject;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.utils.SystemProperty;

@PersistenceAware
public class JSPUtilMethods {

	public static void renderContentsTable(JspWriter out, Key[] contents)
			throws IOException {

		out.write("<table>");
		out.write("<thead><tr><th>Thing Kind</th><th>Thing ID</th></tr></thead>");
		out.write("<tbody>");
		for (Key k : contents) {
			out.write("<tr><td>");
			out.write(k.getKind());
			out.write("</td><td>");
			out.write(getPrettiestNameFromKey(k));
			out.write("</td></tr>");
			if (k.getKind().equals(Planet.class.getSimpleName())) {
				Planet plan = PMF.getPM().getObjectById(Planet.class, k);
				if (plan.contents != null) {
					out.write("<tr><td>Orbiting Planet:</td><td>");
					renderContentsTable(out, plan.contents);
					out.write("</td></tr>");
				}

			}

		}
		out.write("</tbody></table>");
		out.flush();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getPrettiestNameFromKey(Key k) {

		Class c = getNameableClassFromKey(k);
		Nameable n = PMF.getPM().getObjectById(c, k);
		return getPrettiestName(n);
	}

	public static String getPrettiestNameFromKeyNoForm(Key k) {

		Class c = getNameableClassFromKey(k);
		Nameable n = PMF.getPM().getObjectById(c, k);
		return getPrettiestNameNoForm(n);
	}

	@SuppressWarnings("rawtypes")
	public static Class getNameableClassFromKey(Key k) {
		for (Class c : new Class[] { SolarSystem.class, AsteroidBelt.class,
				Moon.class, Planet.class, Station.class, SystemObject.class }) {
			if (c.getSimpleName().equals(k.getKind())) {
				return c;
			}
		}
		return ClassNotFoundException.class;
	}

	public static String getPrettiestName(Nameable n) {
		if (n.getPrettyName() == null) {

			return "<span class=\"red jsAction\" onClick=\"loadBody('GenerateNameRequest.jsp?toName="
					+ KeyFactory.keyToString(n.getKey())
					+ "');\">"
					+ n.getID()
					+ "</span>";
		}
		return n.getPrettyName();
	}

	public static String getPrettiestNameNoForm(Nameable n) {
		if (n.getPrettyName() == null) {
			return n.getID();
		}
		return n.getPrettyName();
	}

	public static boolean isOffline() {
		// System.out.println(SystemProperty.environment.value());
		if (SystemProperty.environment.value().equals(
				SystemProperty.Environment.Value.Development))
			return true;
		return false;
	}

	public static void parseCommand(HttpServletRequest req, JspWriter out)
			throws Exception {

		String[] commands = req.getParameter("command").split("[;,:]");

		String[] symbols = new String[] { // last one need always be argle
		"help", "halp", "generategalaxy", "alertall", "arglegarbleglyopglyph" };

		for (String command : commands) {
			String[] cleanedCommand = command.toLowerCase().split("\\s+");
			int sym = 0;
			for (; sym < symbols.length; sym++) {
				if (symbols[sym].startsWith(cleanedCommand[0])) {
					break;
				}
			}

			switch (sym) {

			case 0:
			case 1:
				// print help
				printHelp(out);

				break;
			case 2:
				// generategalaxy
				SolarSystem[] galaxy;

				PersistenceManager pm = PMF.get().getPersistenceManager();
				galaxy = OfflineDemo.GalaxyGenerator.generateGalaxy(out);
				out.write(galaxy.length + " stars generated.\n");
				try {
					for (SolarSystem s : galaxy)
						pm.makePersistent(s);
				} finally {
					pm.close();
				}
				break;

			default:
				out.write("Unrecognized Command.\n");
				printHelp(out);
			}

		}

	}

	private static void printHelp(JspWriter out) throws IOException {
		out.write("Supported commands:\n");
		out.write("help: print this message.\n");
	}

}
