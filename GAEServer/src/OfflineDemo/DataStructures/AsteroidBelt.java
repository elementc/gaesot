package OfflineDemo.DataStructures;

import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.PersistenceCapable;

@SuppressWarnings("serial")
@PersistenceCapable
@Inheritance(customStrategy = "complete-table")

public class AsteroidBelt extends SystemObject {

	public boolean mineable() {
		return true;
	}

	private long lastMinedTime = System.currentTimeMillis() - 1000 * 10;

	public Item[] mine() {
		if (System.currentTimeMillis() < lastMinedTime + 1000 * 10)
			return new Item[0];
		else {

			lastMinedTime = System.currentTimeMillis();
			Item[] toDrop = new Item[OfflineDemo.StaticSettings.RNG.nextInt(5)];
			for (int i = 0; i < toDrop.length; i++) {
				toDrop[i] = new Ore();
				toDrop[i].name = "Iron";
			}
			return toDrop;

		}
	}

}
