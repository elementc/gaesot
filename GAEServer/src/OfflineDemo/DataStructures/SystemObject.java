/**
 * 
 */
package OfflineDemo.DataStructures;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 * @author cdoran2011
 * 
 */
@SuppressWarnings("serial")
@PersistenceCapable
public abstract class SystemObject implements Serializable, Nameable {

	@Persistent
	protected String id = UUID.randomUUID().toString();
	protected String prettyName;

	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	@PrimaryKey
	protected Key k = KeyFactory.createKey(this.getClass().getSimpleName(), id);

	@Persistent(serialized = "true")
	public Key[] contents;
	protected Key namer;

	public boolean mineable() {
		return false;
	}

	public boolean dockable() {
		return false;
	}

	public Item[] mine() {
		return new Item[0];
	}

	public String toString() {
		return this.getClass().getSimpleName()
				+ id
				+ (contents != null && contents.length > 0 ? " "
						+ Arrays.toString(contents) : "");
	}

	public String getID() {
		return id;
	}

	public Key getKey() {
		return k;
	}

	public String getPrettyName() {
		return prettyName;
	}

	public void setPrettyName(String prettyName) {
		this.prettyName = prettyName;
	}

	public Key getNamer() {
		return namer;
	}

	public void setNamer(Key namer) {
		this.namer = namer;
	}

	public Key[] getContents() {
		return contents;
	}
}
