package OfflineDemo.DataStructures;

import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable
public abstract class Item {
	public String name = "";
	public int value = 0;
	public int size = 0;
	public String toString(){
		return String.format("%s: %s, %d m^3, $%d", this.getClass().getSimpleName(),name,size,value);
	}

}
