package OfflineDemo.DataStructures;

import java.io.Serializable;
import java.util.UUID;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
@PersistenceCapable
public class SolarSystem implements Serializable, Nameable {
	@Persistent
	private String id = UUID.randomUUID().toString();
	private String prettyName;
	@Persistent(serialized = "true")
	public Point loc = new Point(0, 0);
	private boolean isStarterSystem = false;
	private Key namer;

	public boolean isStarterSystem() {
		return isStarterSystem;
	}

	public void markStarterSystem() {
		isStarterSystem = true;
	}

	@Persistent(serialized = "true")
	public Key[] nearbySystems;
	@Persistent(serialized = "true")
	public Key[] contents;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key k = KeyFactory.createKey(this.getClass().getSimpleName(), id);

	public String getID() {
		return id;
	}

	public Key getKey() {
		return k;
	}

	public String getPrettyName() {
		return prettyName;
	}

	public void setPrettyName(String prettyName) {
		this.prettyName = prettyName;
	}

	public Key getNamer() {
		return namer;
	}

	public void setNamer(Key namer) {
		this.namer = namer;
	}

	public String toString() {
		return id;
	}
	public Key[] getContents(){
		return contents;
	}

}
