package OfflineDemo.DataStructures;

import com.google.appengine.api.datastore.Key;

public interface Nameable {
	public void setPrettyName(String name);

	public String getPrettyName();

	public void setNamer(Key k);

	public Key getNamer();

	public String getID();

	public Key getKey();

	public Key[] getContents();

}
