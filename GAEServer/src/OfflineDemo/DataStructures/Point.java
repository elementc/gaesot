package OfflineDemo.DataStructures;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Point implements Serializable {
	public final double x;
	public final double y;

	public Point(double X, double Y) {
		x = X;
		y = Y;
	}

	public double distanceTo(Point p) {
		return Math.sqrt(Math.pow(p.x - x, 2) + Math.pow(p.y - y, 2));
	}

}
