package OfflineDemo.DataStructures;

import java.util.UUID;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(detachable = "true")
public class NameSuggestion {
	@Persistent
	protected String suggestedName;
	@Persistent(serialized = "true")
	protected Key playerWhoSuggested;
	@Persistent(serialized = "true")
	protected Key thingToName;
	@Persistent
	protected ReviewStatus reviewStatus = ReviewStatus.PENDING;
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	protected Key k = KeyFactory.createKey(
			NameSuggestion.class.getSimpleName(), UUID.randomUUID().toString());

	@Override
	public String toString() {
		return String.format("%s -> %s by %s",
				thingToName != null ? thingToName.toString() : "Lost target",
				suggestedName != null ? suggestedName : "Lost Suggested Name",
				playerWhoSuggested != null ? playerWhoSuggested.toString()
						: "Lost player who suggested");
	}

	public String getSuggestedName() {
		return suggestedName;
	}

	public Key getPlayerWhoSuggested() {
		return playerWhoSuggested;
	}

	public Key getThingToName() {
		return thingToName;
	}

	public ReviewStatus getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(ReviewStatus reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public Key getK() {
		return k;
	}

	public NameSuggestion(String suggestion, Key playerkey, Key thingKey) {
		suggestedName = suggestion;
		playerWhoSuggested = playerkey;
		thingToName = thingKey;
	}

	public enum ReviewStatus {
		PENDING, ACCEPTED, DENIED
	}

}
