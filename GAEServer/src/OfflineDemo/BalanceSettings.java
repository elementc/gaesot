package OfflineDemo;

public class BalanceSettings {

	// need properties for all the things that affect the game- drop rates,
	// exchange rates, and so on. might not have much for now, but here's where
	// it'll develop
	// all fields should be private, use getters and setters.
	 private int ironDropRate = 10;
	 private int ironRichness = 5;
	 private int numOfPlayers = 200;
	 private int ironPrice = 50;

	 public int getIronRichness() {
		return ironRichness;
	}

	 public void setIronRichness(int newval) {
		ironRichness = newval;
	}

	 public int getIronDropRate() {
		return ironDropRate;
	}

	 public void setIronDropRate(int ironDropRateNew) {
		ironDropRate = ironDropRateNew;
	}

	 public int getIronPrice() {
		return ironPrice;
	}

	 public void setIronPrice(int ironPriceNew) {
		ironPrice = ironPriceNew;
	}

	 public int getNumOfPlayers() {
		return numOfPlayers;
	}

	 public void setNumOfPlayers(int numOfPlayersNew) {
		numOfPlayers = numOfPlayersNew;
	}
}
