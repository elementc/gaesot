package OfflineDemo;

import java.util.Random;

public class StaticSettings {
	
	static private int seed = 15;
	static public  Random RNG = new Random (seed);
	static private int numOfSystems = 500;
	static private int firstSystem = RNG.nextInt() % numOfSystems;

	static private double jumpDistance = .05;
	//static private double minimumDistance = jumpDistance/2;
	static private int maxFeatures = 10;
	
	
	public static int getSeed() {
		return seed;
	}
	public static void setSeed(int seed) {
		StaticSettings.seed = seed;
		RNG = new Random (seed);
	}
	public static int getMaxFeatures() {
		return maxFeatures;
	}
	public static void setMaxFeatures(int maxFeatures) {
		StaticSettings.maxFeatures = maxFeatures;
	}
	static public int getFirstSystem() {
		return firstSystem;
	}
	static public int getNumOfSystems() {
		return numOfSystems;
	}
	static public void setNumOfSystems(int numOfSystems2) {
		numOfSystems = numOfSystems2;
	}

	static public double getJumpDistance() {
		return jumpDistance;
	}
	static public void setJumpDistance(double jumpDistance2) {
		jumpDistance = jumpDistance2;
	}

	
	// in here, manage stuff related to the static generation of the galaxy.
		// number of planets, jump distance, and so on. You'll have a lot of these.
		// Getters and setters, plz.
	
}
