package OfflineDemo;

import java.io.Serializable;
import java.util.UUID;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
@PersistenceCapable
public class Player implements Serializable {

	public Key getLocation() {
		return location;
	}

	public void setLocation(Key location) {
		this.location = location;
	}

	public Key getDockedLocation() {
		return dockedLocation;
	}

	public void setDockedLocation(Key dockedLocation) {
		this.dockedLocation = dockedLocation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Key getKey() {
		return k;
	}

	public void setKey(Key k) {
		this.k = k;
	}

	public long getFacebook_user_id() {
		return facebookUserID;
	}

	public void setFacebook_user_id(long facebook_user_id) {
		this.facebookUserID = facebook_user_id;
	}

	public String getoAuthToken() {
		return oAuthToken;
	}

	public void setoAuthToken(String oAuthToken) {
		this.oAuthToken = oAuthToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public long getOreMinedTotal() {
		return oreMinedTotal;
	}

	public void setOreMinedTotal(long oreMinedTotal) {
		this.oreMinedTotal = oreMinedTotal;
	}

	@Persistent
	private Key location;
	@Persistent
	private Key dockedLocation;
	@Persistent
	private String id = UUID.randomUUID().toString();
	@Persistent
	private long facebookUserID = -1;
	@Persistent
	private String oAuthToken;
	@Persistent
	private String userName;
	@Persistent
	private boolean isAdmin = false;
	
	@Persistent
	private long oreMinedTotal = 0;

	@Persistent
	@PrimaryKey
	private Key k = KeyFactory.createKey(this.getClass().getSimpleName(), id);

}
