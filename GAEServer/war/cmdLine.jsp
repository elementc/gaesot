<%@page import="com.cse2010.group1.JSPUtilMethods"%>
<%@page import="com.google.appengine.api.users.UserService"%>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>

<%
	String URI = "";

	if (JSPUtilMethods.isOffline()) {
		//in test, who gives a hoot.
		URI = "/cmdLine.jsp";

	} else {

		URI = "https://cmdline.cse2010group1.appspot.com/cmdLine.jsp";

		//in production, require users be Casey.
		if (request.getUserPrincipal() == null
				|| !request.getUserPrincipal().equals(
						"wer4geeks@gmail.com")) {
			UserService userService = UserServiceFactory
					.getUserService();
			response.sendRedirect(userService.createLoginURL(request
					.getRequestURI()));
			return;
		}
	}
%>

<h1>Command Line</h1>

<form id="cmdLine">
	<input type="text" size="50%" name="command" value="commands" /><input
		type="submit" value="Execute" />
</form>

<script>
		$("#cmdLine").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault(); 
    
    postBody('<%out.write(URI);%>', $('#cmdLine').serialize());
    });
    </script>

<div id="console">
	<%
		if (request.getParameter("command") == null) {
			out.write("No command.");
		} else {
			JSPUtilMethods.parseCommand(request, out);

		}
	%>

</div>