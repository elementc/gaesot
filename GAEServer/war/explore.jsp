<%@page import="OfflineDemo.*"%>
<%@page import="OfflineDemo.DataStructures.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="com.google.appengine.api.datastore.*"%>

<%
	if (session.getAttribute("player") == null) {
		response.getWriter().println("<script>loadBody('explore');</script>");
		return;
	}
%>


<h1>Galaxy Exploration</h1>
<br />
<div class="container">
	<div class="leftbox">
		<div class="center">
			<%
				Player p = PMF.getPM().getObjectById(Player.class,
						session.getAttribute("player"));
				SolarSystem s = PMF.getPM().getObjectById(SolarSystem.class,
						p.getLocation());

				out.write("<b>Current System: </b>"
						+ JSPUtilMethods.getPrettiestName(s));
			%>
		</div>
		<table>
			<thead>
				<tr>
					<th>Nearby Systems (Click to Jump)</th>
				</tr>

			</thead>
			<%
				for (Key k : s.nearbySystems) {
					out.write("<tr><td>");
					out.write("<span class=\"jsAction\" onClick=\"loadBody('explore?jumpTarget=");
					out.write(k.getName());
					out.write("');\">");
					out.write(JSPUtilMethods.getPrettiestNameFromKeyNoForm(k));
					out.write("</span></td></tr>");
				}
			%>
		</table>
	</div>

	<div class="rightbox">
		<table>
			<thead>
				<tr>
					<th>Orbiting This System</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<%
							JSPUtilMethods.renderContentsTable(out, s.contents);
						%>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>