<%@page import="com.cse2010.group1.JSPUtilMethods" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>The Howling Black</title>
<link rel="stylesheet" href="style.css" type="text/css" />
<script src="jquery-1.7.1.min.js"></script>
<script>
function loadBody(target){
$("#body").fadeOut(250, function(){
	$("#body").load(target, function(){
			$("#body").fadeIn(250);
		});
	});
}

function postBody(target, data){
$("#body").fadeOut(250, function(){
		$("#body").load(target, data, function(){
			$("#body").fadeIn(250);
		});
	});
}

</script>
</head>
<body>
	<div class="container">
		<div class="leftcolumn">
			<h2 id="the" onClick="loadBody('status.html');">THE</h2>
			<h2 id="howling" onClick="loadBody('status.html');">HOWLING</h2>
			<h2 id="black" onClick="loadBody('status.html');">BLACK</h2>
			<ul>
				<li><span class="jsAction" onClick="loadBody('explore');">Explore</span></li>
				<li><span class="jsAction"
					onClick="loadBody('PlayerStats.jsp');">Player Stats</span></li>
				<li>Galaxy Map(Under Construction)</li>
				<li><span class="jsAction"
					onClick="loadBody('MiningGame.jsp');">Mining Game Preview</span></li>
				<%
					if (session.getAttribute("isAdmin") != null
							&& session.getAttribute("isAdmin").equals("true")) {
						if (JSPUtilMethods.isOffline()){
						out.write("<li><span class=\"jsAction\" onClick=\"loadBody('/cmdLine.jsp');\">Command Line</span></li>\n");
						}
						else{
							out.write("<li><span class=\"jsAction\" onClick=\"loadBody('https://cmdline.cse2010group1.appspot.com/cmdLine.jsp');\">Command Line</span></li>\n");
						}
						out.write("<li><span class=\"jsAction\" onClick=\"loadBody('ManageNameRequests.jsp');\">Manage Name Requests</span></li>\n");
					}
				%>
			</ul>
		</div>
		<div class="rightcolumn">