<%@page import="OfflineDemo.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="com.google.appengine.api.datastore.*"%>
<%@page import="javax.jdo.*"%>
<%@page import="javax.jdo.Query"%>
<%@page import="java.util.List"%>

<%
	if (request.getParameter("signed_request") == null) {
		return;
	}
	FacebookSignedRequest SR = FacebookSignedRequest
			.getFacebookSignedRequest(request
					.getParameter("signed_request"));
	Query q = PMF.getPM().newQuery(Player.class);
	q.setFilter("facebookUserID == param1");
	q.declareParameters("long param1");
	List<Player> players = (List<Player>) q.execute(SR.getUser_id());
	if (players.size() == 1) {
		//player exists, grab them
		Player p = players.get(0);
		p.setAdmin(false);
		p.setFacebook_user_id(-1);
		p.setUserName("A long forgotten adventurer");
		p.setoAuthToken(null);	
		PMF.getPM().makePersistent(p);
		session.setAttribute("player", null);

	} else {
		//punt
		throw new Exception(
				"OMFG! More than one player object in the datastore with this ID.");
	}
%>