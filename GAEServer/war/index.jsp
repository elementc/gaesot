<%@page import="OfflineDemo.*"%>
<%@page import="OfflineDemo.DataStructures.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="com.google.appengine.api.datastore.*"%>
<%@page import="javax.jdo.Query"%>

<%@page import="com.google.appengine.api.utils.SystemProperty"%>

<%@page import="javax.jdo.*"%>
<%@page import="javax.jdo.Query"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="com.restfb.*"%>
<%@page import="com.restfb.types.*"%>
<%
	String signedRequest = (String) request
			.getParameter("signed_request");

	PersistenceManager PM = PMF.getPM();

	FacebookSignedRequest FacebookSR = null;
	String oauthToken = null;
	long userIDNumber = -1;
	Player p = null;

	if (JSPUtilMethods.isOffline()) {
		//in development mode, fake it
		signedRequest = "DLcp9YVQ4vg21hijnJpSTR1Ju1IdmNOZKosePaTaPXo.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImV4cGlyZXMiOjEzNTQ2OTQ0MDAsImlzc3VlZF9hdCI6MTM1NDY4ODk3Nywib2F1dGhfdG9rZW4iOiJBQUFHTk1kaVJpeTRCQUszWkF5TEhoTVFNUWZtS3ZrQXVUTFJUaE8wWFdNVEgzSnhscmEzY1FwcEk0V0N0MmtkaFpBV1JDdmNHdUZGRmZPQVBPenRva1Z4YkdydXo3MTVjMmEzdGJYWkFiNUZSamFVa1pDZEUiLCJ1c2VyIjp7ImNvdW50cnkiOiJ1cyIsImxvY2FsZSI6ImVuX1VTIiwiYWdlIjp7Im1pbiI6MTgsIm1heCI6MjB9fSwidXNlcl9pZCI6IjE2Mjk2ODU2NzcifQ";
		//throw new Exception("You're debugging my bugger!");
		FacebookSR = new FacebookSignedRequest();
		oauthToken = "LOL_DISREGARD";
		userIDNumber = -900841128;
		FacebookSR.setOauth_token(oauthToken);
		FacebookSR.setUser_id(userIDNumber);

	} else {
		if (signedRequest == null) {
			//throw new Exception("You're debugging my bugger!");
			response.getWriter()
					.write("<!doctype html><html><head><script> top.location.href='https://apps.facebook.com/spaceoregontrail/'</script></head></html>");
			return;
		} else {
			//we have a request, create the requisite objects
			try {
				FacebookSR = FacebookSignedRequest
						.getFacebookSignedRequest(signedRequest);
				oauthToken = FacebookSR.getOauth_token();
				userIDNumber = FacebookSR.getUser_id();
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}

	//attempt to get the session's Player object.
	Key ID = null;
	try {
		ID = (Key) session.getAttribute("player");
	} catch (Exception e) {
		session.setAttribute("player", null);

	}
	if (ID != null) {
		//we have a player associated with the request. get them!
		try {
			p = PM.getObjectById(Player.class, ID);
		} catch (Exception ex) {

		}

	}

	if (p == null) {
		//we have a request, but no player in the session. 
		//see if we can pull one from the datastore.
		Query q = PM.newQuery(Player.class);
		q.setFilter("facebookUserID == param1");
		q.declareParameters("long param1");
		List<Player> players = (List<Player>) q.execute(FacebookSR
				.getUser_id());
		if (players.size() < 1) {
			//player doesn't exist! create a new one!
			p = new Player();
			p.setFacebook_user_id(FacebookSR.getUser_id());
		} else if (players.size() == 1) {
			//player exists, grab them
			p = players.get(0);
		} else {
			//punt
			throw new Exception(
					"OMFG! More than one player object in the datastore with this ID.");
		}

		//always update the oauth token... we want to have the latest one.
		p.setoAuthToken(FacebookSR.getOauth_token());

		FacebookClient facebookClient = null;
		User user = null;
		if (JSPUtilMethods.isOffline()) {
			p.setUserName("Casey Doran");
		} else {
			facebookClient = new DefaultFacebookClient(
					p.getoAuthToken());
			user = facebookClient.fetchObject("me", User.class);
			p.setUserName(user.getName());
		}

		if (p.getFacebook_user_id() == 1629685677
				|| (p.getFacebook_user_id() == -900841128 && JSPUtilMethods
						.isOffline())) {
			p.setAdmin(true);
		}

		PM.makePersistent(p);
		if (p.isAdmin()) {
			session.setAttribute("isAdmin", "true");
		} else
			session.setAttribute("isAdmin", "false");
		session.setAttribute("player", p.getKey());
	}
%>

<jsp:include page="page_header.jsp" />
<br />
<div id="body"></div>
<script>
$(document).ready(function(){
  
  $("#body").load("status.html");
  
 });
</script>



<jsp:include page="page_footer.html" />

<!-- Your Signed Request:  <%out.write(signedRequest);%>  -->

<%
	PM.close();
%>
