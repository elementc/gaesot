<%@page import="OfflineDemo.*"%>
<%@page import="OfflineDemo.DataStructures.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="com.google.appengine.api.datastore.*"%>
<%@page import="com.google.appengine.api.users.*"%>
<%@page import="javax.jdo.*"%>
<%@page import="javax.jdo.Query"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%
	if (session.getAttribute("isAdmin") == null
			|| !session.getAttribute("isAdmin").equals("true")) {
		response.sendRedirect("/");
		return;
	}

	int startIndex = 0;
	int requestedIndex = (request.getParameter("startIndex") != null ? Integer
			.parseInt(request.getParameter("startIndex")) : 0);
	startIndex = Math.max(requestedIndex, startIndex);
	PersistenceManager PM = PMF.getPM();
	Query q = PM.newQuery(NameSuggestion.class);
	q.setFilter("reviewStatus == 'PENDING'");
	//q.setOrdering("thingToName desc");
	q.setRange(startIndex, startIndex + 20);

	//need a way to get fields while detached :(
	PM.setDetachAllOnCommit(true);

	List<NameSuggestion> suggestions = (List<NameSuggestion>) q
			.execute();
	PM.close();
%>

<h1>Manage Name Requests</h1>
<br />
<%
	if (startIndex > 0) {
		out.write("<span onClick=\"loadBody('ManageNameRequests.jsp?startIndex="
				+ (startIndex - 20 >= 0 ? startIndex - 20 : 0)
				+ "');\">&lt;Previous 20</span>");
	}
%>
<%
	if (!(suggestions.size() < 20)) {
		out.write("|<span onClick=\"loadBody('ManageNameRequests.jsp?startIndex="
				+ (startIndex + 20) + "');\">Next 20&gt;</span>");
	}
%>
<form id="requestsAction" action="/applyNameRequest" method="post">
	<table>
		<thead>
			<tr>
				<th>Selected</th>
				<th>#</th>
				<th>Suggested Name</th>
				<th>Target ID</th>
				<th>Target Type</th>
				<th>Player</th>
			</tr>
		</thead>
		<tbody>
			<%
				int counter = startIndex;
				for (NameSuggestion n : suggestions) {

					out.write("<tr>");
					out.write("<td>");
					out.write("<input type=\"checkbox\" name=\""
							+ KeyFactory.keyToString(n.getK()) + "\" />");
					out.write("</td><td>");
					out.write(Integer.toString(counter));
					out.write("</td><td>");
					out.write(n.getSuggestedName() == null ? "BLANK_NAME" : n
							.getSuggestedName());
					out.write("</td><td>");
					out.write(n.getThingToName().getName());
					out.write("</td><td>");
					out.write(n.getThingToName().getKind());
					out.write("</td><td>");
					out.write(n.getPlayerWhoSuggested().getName());
					out.write("</td></tr>");
					counter++;
				}
			%>
		</tbody>
	</table>
	<input type="submit" name="approve" value="Approve" id="approve" /> <input
		type="submit" name="deny" value="Deny" id="deny" />
</form>

<script>
$("#requestsAction").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault(); 
    
    postBody('/applyNameRequest', $('#requestsAction').serialize());
    });
</script>
