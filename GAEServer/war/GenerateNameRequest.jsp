<%@page import="OfflineDemo.*"%>
<%@page import="OfflineDemo.DataStructures.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="com.google.appengine.api.datastore.*"%>
<%
	if (request.getParameter("toName") == null) {
		response.sendRedirect("explore.jsp");
	}
	Key k = null;
	Nameable n = null;
	try {
		k = KeyFactory.stringToKey(request.getParameter("toName"));
		@SuppressWarnings({ "rawtypes" })
		Class c = JSPUtilMethods.getNameableClassFromKey(k);
		n = (Nameable) PMF.getPM().getObjectById(c, k);
	} catch (Exception e) {
		response.sendRedirect("explore.jsp");
	}
%>

<h1>Name an Object</h1>
<br />
<p>
	Congratulations, explorer! You've found a stellar object, ISU SOD#
	<%
	out.write(k.getName());
%>, of class
	<%
	out.write(k.getKind());
%>. This is your big chance to make an impact on history, by submitting
	a potential name to the ISU Stellar Object Name Review Board.
</p>
<div class="container">
	<div class="leftbox">
		<h2>Suggestion form</h2>
		<form id="nameSuggestion">
			<input type="hidden" name="toName"
				value="<%out.write(KeyFactory.keyToString(k));%>" /> <input
				type="text" size="30px" name="suggestedName"
				value="Your Suggested Name Here" /><input type="submit"
				value="Submit Name Suggestion"  />
		</form>

		<script>$("#nameSuggestion").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault(); 
    
    postBody('/submitName', $('#nameSuggestion').serialize());
    });
    </script>
	</div>
	<div class="rightbox">
		<%
			if (n.getContents() != null) {
				out.write("<h2>System Information</h2><br />");
				JSPUtilMethods.renderContentsTable(out, n.getContents());

			}
		%>
	</div>


</div>

