<%@page import="OfflineDemo.*"%>
<%@page import="OfflineDemo.DataStructures.*"%>
<%@page import="com.cse2010.group1.*"%>
<%@page import="javax.jdo.*"%>
<%@page import="javax.jdo.Query"%>
<%@page import="com.google.appengine.api.datastore.*"%>
<%@page import="java.util.List"%>
<%
	Player p = PMF.getPM().getObjectById(Player.class,
			(Key) session.getAttribute("player"));
%>

<h1>Player Statistics</h1>
<table>
	<thead>
		<tr>
			<th>Field</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Player Name</td>
			<td>
				<%
					out.write(p.getUserName());
				%>
			</td>
		</tr>
		<tr>
			<td>Player ID</td>
			<td>
				<%
					out.write(p.getId());
				%>
			</td>
		</tr>
		<tr>
			<td>Player Location ID</td>
			<td>
				<%
					out.write(p.getLocation().getName());
				%>
			</td>
		</tr>
		<tr>
			<td>Player Facebook ID</td>
			<td>
				<%
					out.write(Long.toString(p.getFacebook_user_id()));
				%>
			</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th>Suggested Name</th>
			<th>Status</th>
			<th>Object ID</th>
		</tr>
	</thead>
	<tbody>
		<%
			Query q = PMF.getPM().newQuery(NameSuggestion.class);
			q.setFilter("playerWhoSuggested == :player");
			List<NameSuggestion> suggestions = (List<NameSuggestion>) q
					.execute(p.getKey());
			for (NameSuggestion n : suggestions) {
				out.write("<tr><td>" + n.getSuggestedName() + "</td><td>"
						+ n.getReviewStatus().toString() + "</td><td>"
						+ n.getThingToName().getName() + "</td></tr>");
			}
		%>


	</tbody>
</table>
